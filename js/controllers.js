'use strict';

/* Controllers */

var userApp = angular.module('userApp', ['ngFileUpload']);

userApp.config(function($httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
});



userApp.controller('userAppCtrl', ['$scope', '$http', 'Upload','$timeout',
function($scope, $http,Upload, $timeout) {

$scope.setErrorFalse= function func(){
    $scope.errorMsg=false;
  }
  $scope.setErrorFalse();

var successCallback=function successCallback(response) {
      // this callback will be called asynchronously
      // when the response is available
    //  angular.fromJson(json);
    $scope.userDetails=response.data;
    $scope.selectedImage=response.data.pictureURL;

    }
    var errorCallback=function errorCallback(response) {
      $scope.userDetails=response.data;
      $scope.errorMsg=response.data.errorMessage;
      //alert("bye"+angular.toJson(response));
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    }
    $http.get('https://inigoapp.com/team/User/GetById/81561').then(successCallback, errorCallback);
    $scope.send= function sendPost(user){
                  //  alert(angular.toJson(user));
                    // var info = {};
                    // info.AccountID =user.accountID;
                    // info.DisplayName =user.displayName ;
                    // info.UserName  =user.userName  ;
                    // info.PictureURL  =user.pictureURL  ;
                    //  info.Description  =user.description  ;
                    // alert('info::'+angular.toJson(info));
                    //https://inigoapp.com/team/User/Edit',

                  //  var me={"accountID":81561,"displayName":"Manor Lahagani","userName":"manor21@gmail.com","description":"manorrr","pictureURL":"https://inigoapp.com/m/images/avatar_10.png","createdOn":"2015-12-27T17:08:41.947","createdBy":"InigoTeamUser:pavelkir"};

                    $http({
                            url: 'https://inigoapp.com/team/User/Edit',
                            method: "POST",
                            data: angular.toJson(user),
                            headers: {'content-type': 'application/json'}
                        }).success(function (data, status, headers, config) {
                          $scope.finish=true;
                          // alert('success');
                        }).error(function (data, status, headers, config) {
                          if(data==null){
                              $scope.errorMsg='there is no data response';
                            }else {
                              $scope.errorMsg='status'+status+' '+data.errorMessage;

                            }
                          // alert('failed '+data)
                        });

    }


        $scope.updatePicture=function updatePic(picUrl){
          $scope.userDetails.pictureURL=picUrl;
        }
        $scope.SetSelectedImage=function SetSelectedImage(picUrl){
          $scope.selectedImage=picUrl;
        }

    //call 3
      var loadPictures=function call3(){
                  $http({
                          method: 'GET',
                          url: 'https://inigoapp.com/team/user/GetProfilePictures/81561'
                        }).then(function successCallback(response) {
                            // this callback will be called asynchronously
                            // when the response is available
                            $scope.pictures=response.data;
                          }, function errorCallback(response) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                            alert('error'+ response.data);
                          });

                        }
              loadPictures();

//upload file
$scope.uploadFiles = function(file, errFiles,userDetails) {
       $scope.f = file;
       $scope.errFile = errFiles && errFiles[0];
//alert(file);
      var flag=true;
      if(file==null || file.type==null || file.type.indexOf('image')!=0){
          $scope.errorMsg= 'choose only images';
          flag=false;
        }
       if (file && flag) {
             var info={"accountId":"","fileName":"","currentUsername":""};
             info.accountId=userDetails.accountID;
             info.fileName=file.name;
             info.currentUsername=userDetails.userName;
              file.upload = Upload.upload({
               url: 'https://inigoapp.com/team/api/file/UploadProfilePicture',
              method: 'POST',
              data: {file: file,AddImageForm: Upload.json(info)}

           });

           file.upload.then(function (response) {
               $timeout(function () {
                  //the json in response is in Upercase
                  //so i will load again
                  loadPictures();
               });
           }, function (response) {
              if (response.status > 0)
                   $scope.errorMsg = response.status + ': ' + response.data;
              else{
                    $scope.errorMsg='cors error';
                  }
           }, function (evt) {
               file.progress = Math.min(100, parseInt(100.0 *
                                        evt.loaded / evt.total));
           });
       }
   }

      $scope.isPressed= function funcpic(url){
            return $scope.selectedImage==url;
          }

}]);
